from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re

PATH = "/home/israel/dev/libs/chromedriver"
driver = webdriver.Chrome(PATH)

driver.get('https://ais.usvisa-info.com/es-mx/niv/users/sign_in')

try:
    email = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "user_email"))
    )
    email.send_keys("")
    driver.find_element_by_id("user_password").send_keys("")
    check = driver.find_element_by_id("policy_confirmed")
    driver.execute_script("arguments[0].click;", check)
    driver.find_element_by_name("commit").click()
    time.sleep(3)

    count = 1
    while count < 1000:
        driver.get('https://ais.usvisa-info.com/es-mx/niv/schedule/35023171/appointment/days/88.json?&consulate_id=74&consulate_date=$consulate_time=&appointments[expedite]=false')
        content = driver.page_source
        reg = re.search("(\d{4})[-](\d{2})[-](\d{2})", content).group(0)
        print(reg)
        datefound = reg.slipt("-")
        year = int(datefound[0])
        month = int(datefound[1])
        day = int(datefound[2])
        if (year < 2023):
            if (month <= 1):
                #do something
                pass
        count+=1
        time.sleep(60)                
finally:
    driver.close()
    driver.quit()
