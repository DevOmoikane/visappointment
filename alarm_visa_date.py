import time
import re
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#probablemente tengas que instalar esta librería de acá abajo
from playsound import playsound


#Tienes que bajar el selenium chromedriver y poner el path acá
driver = webdriver.Chrome('/home/israel/dev/libs/chromedriver')  

driver.get('https://ais.usvisa-info.com/es-mx/niv/users/sign_in')

driver.find_element_by_id("user_email").send_keys("psic.perla.flores@gmail.com")
driver.find_element_by_id("user_password").send_keys("Perl3319")
check = driver.find_element_by_id("policy_confirmed")
driver.execute_script("arguments[0].click();", check)
driver.find_element_by_name("commit").click()
time.sleep(3)
driver.find_element_by_xpath('//a[contains(@href,"continue_actions")]').click()
time.sleep(3)
current_url = driver.current_url
print(current_url)
urlid = re.search("schedule\/(\d+)\/continue_actions", current_url).group(1)

def found_appointment(date):
    playsound('/home/israel/dev/resources/victory_6.mp3')
    driver.get('https://ais.usvisa-info.com/es-mx/niv/schedule/'+urlid+'/appointment')
    select = Select(driver.find_element_by_id('appointments_consulate_appointment_facility_id'))
    select.select_by_value('74')
    time.sleep(2)
    driver.find_element_by_id("appointments_asc_appointment_date").click()
    time.sleep(1)
    calendarelements = driver.find_elements_by_xpath(".//*[@id='ui-datepicker-div']/table/tbody/tr/td/a")
    for dates in calendarelements:
        if(dates.is_enabled() and dates.is_displayed() and str(dates.get_attribute("innerText")) == reg):
            dates.click()
    time.sleep(1)
    timeselect = Select(driver.find_element_by_id('appointments_asc_appointment_time'))
    timeselect.select_by_index(1)
    time.sleep(1)
    #driver.find_element_by_id("appointments_submit").click()
    time.sleep(360)

count = 1
driver.get('https://ais.usvisa-info.com/es-mx/niv/schedule/'+urlid+'/appointment')
time.sleep(10)
try:
    while count < 1000:
        driver.get('https://ais.usvisa-info.com/es-mx/niv/schedule/'+urlid+'/appointment/days/88.json?&consulate_id=74&consulate_date=&consulate_time=&appointments[expedite]=false')
        content = driver.page_source
        reg = re.search("(\d{4})[-](\d{2})[-](\d{2})", content).group(0)
        print("Count: " + str(count) + " at " + time.strftime("%H:%M:%S", time.localtime()))
        print(reg)
        year = int(reg.split("-")[0])
        month = int(reg.split("-")[1])
        day = int(reg.split("-")[2])
        if (year < 2023):
            if (month <= 4):
                found_appointment(reg)
        count+=1
        time.sleep(60)
except:
    #playsound('/home/israel/dev/resources/sadTrombone.mp3')    
    driver.close()
    driver.quit()
